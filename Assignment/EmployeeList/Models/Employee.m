//
// Created by Nick Kibish on 2019-01-18.
// Copyright (c) 2019 Nick Kibish. All rights reserved.
//

#import "Employee.h"

static NSUInteger const kStartingSalary = 10000;
NSString* const kSalaryCurrency = @"EUR";

@implementation Employee

- (instancetype)initWithName:(NSString *)name birthYear:(NSUInteger)birthYear {
    self = [super init];
    if(self) {
        _name = name;
        _birthYear = birthYear;
        _salary = [[NSDecimalNumber alloc] initWithUnsignedInteger:kStartingSalary];
    }
    return self;
}

- (NSString *)readableSalary {
    NSNumberFormatter *formatter = [NSNumberFormatter new];
    formatter.currencyCode = kSalaryCurrency;
    formatter.numberStyle = NSNumberFormatterCurrencyStyle;
    formatter.minimumFractionDigits = 2;
    return [formatter stringFromNumber:self.salary];
}


@end
//
// Created by Nick Kibish on 2019-01-18.
// Copyright (c) 2019 Nick Kibish. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface Employee : NSObject


@property (readonly, copy) NSString* name;
@property (readonly) NSUInteger birthYear;
@property (readonly, copy) NSDecimalNumber* salary;
@property (readonly) NSString *readableSalary;

- (instancetype)initWithName:(NSString*)name birthYear:(NSUInteger)birthYear;

@end
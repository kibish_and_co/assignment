//
// Created by Nick Kibish on 2019-01-18.
// Copyright (c) 2019 Nick Kibish. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Employee.h"

// notification posted when the directory finishes updating
extern NSString* const kEmployeeDirectoryDidUpdateNotification;

@interface EmployeeDirectory : NSObject
@property (readonly) NSArray<Employee *>* employees; // returns NSArray of Employee
@property (readonly) BOOL isUpdating;

- (void)update;

- (void)sort;

@end

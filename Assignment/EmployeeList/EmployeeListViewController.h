//
// Created by Nick Kibish on 2019-01-18.
// Copyright (c) 2019 Nick Kibish. All rights reserved.
//

@import UIKit;

@class EmployeeListDataSource;
@class EmployeeDirectory;

@interface EmployeeListViewController : UITableViewController

- (instancetype)initWithDataProvider:(EmployeeDirectory *)directory dataService:(EmployeeListDataSource *)dataSource;

@end
//
// Created by Nick Kibish on 2019-01-18.
// Copyright (c) 2019 Nick Kibish. All rights reserved.
//

#import "EmployeeCell.h"


@implementation EmployeeCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(nullable NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setupUI];
    }

    return self;
}

- (void)setupUI {
    NSArray *constraints = @[
            [self.name.topAnchor constraintEqualToAnchor:self.contentView.layoutMarginsGuide.topAnchor],
            [self.name.leadingAnchor constraintEqualToAnchor:self.contentView.leadingAnchor constant:16],

            [self.birthday.topAnchor constraintEqualToAnchor:self.name.bottomAnchor constant:8],
            [self.birthday.leadingAnchor constraintEqualToAnchor:self.name.leadingAnchor],
            [self.birthday.bottomAnchor constraintEqualToAnchor:self.contentView.layoutMarginsGuide.bottomAnchor],

            [self.salery.centerYAnchor constraintEqualToAnchor:self.name.centerYAnchor],
            [self.salery.trailingAnchor constraintEqualToAnchor:self.contentView.layoutMarginsGuide.trailingAnchor]
    ];

    for (NSLayoutConstraint *constraint in constraints) {
        [constraint setActive:YES];
    }
}

- (UILabel *)name {
    if (_name) {
        return _name;
    }

    _name = [UILabel new];
    _name.font = [UIFont systemFontOfSize:20 weight:UIFontWeightBlack];
    _name.translatesAutoresizingMaskIntoConstraints = NO;
    [self.contentView addSubview:_name];

    return _name;
}

- (UILabel *)birthday {
    if (_birthday) {
        return _birthday;
    }

    _birthday = [UILabel new];
    _birthday.font = [UIFont systemFontOfSize:13 weight:UIFontWeightThin];
    _birthday.translatesAutoresizingMaskIntoConstraints = NO;
    [self.contentView addSubview:_birthday];

    return _birthday;
}

- (UILabel *)salery {
    if (_salery) {
        return _salery;
    }

    _salery = [UILabel new];
    _salery.translatesAutoresizingMaskIntoConstraints = NO;
    [self.contentView addSubview:_salery];

    return _salery;
}


@end

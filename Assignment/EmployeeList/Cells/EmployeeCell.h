//
// Created by Nick Kibish on 2019-01-18.
// Copyright (c) 2019 Nick Kibish. All rights reserved.
//

@import UIKit;

@interface EmployeeCell : UITableViewCell

@property (strong, nonatomic) UILabel *name;
@property (strong, nonatomic) UILabel *birthday;
@property (strong, nonatomic) UILabel *salery;

@end
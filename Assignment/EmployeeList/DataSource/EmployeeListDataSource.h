//
// Created by Nick Kibish on 2019-01-18.
// Copyright (c) 2019 Nick Kibish. All rights reserved.
//

@import UIKit;

@class Employee;

@interface EmployeeListDataSource : NSObject <UITableViewDataSource>

@property (strong, nonatomic) NSArray <Employee *>* employees;

@end
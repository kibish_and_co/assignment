//
// Created by Nick Kibish on 2019-01-18.
// Copyright (c) 2019 Nick Kibish. All rights reserved.
//

#import "EmployeeCell.h"
#import "Employee.h"
#import "EmployeeListDataSource.h"

@implementation EmployeeListDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.employees.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    EmployeeCell *cell = [tableView dequeueReusableCellWithIdentifier:@"EmployeeCell"];
    Employee *employee = self.employees[indexPath.row];

    cell.name.text = employee.name;
    cell.birthday.text = [NSString stringWithFormat:@"%ld", employee.birthYear];
    cell.salery.text = employee.readableSalary;

    return cell;
}


@end

//
// Created by Nick Kibish on 2019-01-18.
// Copyright (c) 2019 Nick Kibish. All rights reserved.
//

#import "EmployeeListViewController.h"
#import "EmployeeCell.h"
#import "EmployeeListDataSource.h"
#import "EmployeeDirectory.h"

@interface EmployeeListViewController ()
@property (strong, nonatomic) EmployeeListDataSource *dataSource;
@property (strong, nonatomic) EmployeeDirectory *directory;
@end

@implementation EmployeeListViewController {
    UIActivityIndicatorView *_activity;
}

- (instancetype)initWithDataProvider:(EmployeeDirectory *)directory dataService:(EmployeeListDataSource *)dataSource; {
    self = [super init];
    if (self) {
        self.dataSource = dataSource;
        self.directory = directory;
    }

    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupTableView];
    [self setupNavigationItem];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadData) name:kEmployeeDirectoryDidUpdateNotification object:nil];
    [self.directory update];
}

- (void)reloadData {
    self.dataSource.employees = self.directory.employees;
    [self.tableView reloadData];
    [self setNavigationButtonsEnabled:!self.directory.isUpdating];
}

- (void)setupTableView {
    self.tableView.dataSource = self.dataSource;
    [self.tableView registerClass:[EmployeeCell class] forCellReuseIdentifier:@"EmployeeCell"];
    [self.tableView setRowHeight:UITableViewAutomaticDimension];
}

- (void)setupNavigationItem {
    self.navigationItem.title = @"Employee List";

    UIBarButtonItem *update = [[UIBarButtonItem alloc] initWithTitle:@"Update" style:UIBarButtonItemStyleDone target:self action:@selector(update)];
    
    UIBarButtonItem *sort = [[UIBarButtonItem alloc] initWithTitle:@"Sort" style:UIBarButtonItemStyleDone target:self action:@selector(sort)];
    
    self.navigationItem.rightBarButtonItem = update;
    self.navigationItem.leftBarButtonItem = sort;
    
    [self setNavigationButtonsEnabled:NO];
}

- (void)update {
    [self setNavigationButtonsEnabled:NO];
    [self.directory update];
}

- (void)sort {
    [self setNavigationButtonsEnabled:NO];
    [self.directory sort];
}

- (void)setNavigationButtonsEnabled:(BOOL)isEnabled {
    [self.navigationItem.leftBarButtonItem setEnabled:isEnabled];
    [self.navigationItem.rightBarButtonItem setEnabled:isEnabled];
}

@end

//
//  main.m
//  Assignment
//
//  Created by Nick Kibish on 1/18/19.
//  Copyright © 2019 Nick Kibish. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}

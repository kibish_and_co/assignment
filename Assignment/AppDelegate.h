//
//  AppDelegate.h
//  Assignment
//
//  Created by Nick Kibish on 1/18/19.
//  Copyright © 2019 Nick Kibish. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end


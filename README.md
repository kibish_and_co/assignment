# Assignment

## Part 1
0. `T1` isn't informative name for class.
1. That's not good idea to have a static variable for `savedCallback`. It would be better to have different block for each invocation. As we have only the one stored block we lose all previous results got from this method.
2. `ProgressBar` should be updated from main thread (as well as all UI). Also it would be better to pass it to method as a parameter.
3. `performSelectorInBackground:` has no good performance as it create new thread for each execution and that's pricey operation. That's better to use moderner approach like GCD.
4. If we do some long work with one object we should consider that fact that it can be changed in another thread. So we need to provide thread-safe mechanism or work with its copy.

## Part 2
I created subclass of `UITableViewController` for displaying list of employees. I didn't use XIBs or Storyboards for UI and created own `UITableViewCell` with custom layout.

I used KVO for detecting updating and sorting process.

The only doubt I have is about point 6. I ran my code on device and didn't notice any lug. 
